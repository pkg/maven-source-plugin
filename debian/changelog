maven-source-plugin (3.2.1-2+apertis1) apertis; urgency=medium

  * Move package to development repository. Needed for the Java suite

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Tue, 10 Oct 2023 22:07:05 +0530

maven-source-plugin (3.2.1-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 05 Oct 2023 17:38:04 +0000

maven-source-plugin (3.2.1-2) unstable; urgency=medium

  * Depend on libmaven-parent-java (Closes: #1028831)
  * Standards-Version updated to 4.6.2
  * Removed Thomas Koch from the uploaders (Closes: #1019034)

 -- Emmanuel Bourg <ebourg@apache.org>  Sun, 15 Jan 2023 10:46:49 +0100

maven-source-plugin (3.2.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Depend on libmaven-archiver-java (>= 3.6)
    - Updated the Maven rules
  * No longer install the jar in /usr/share/java
  * Standards-Version updated to 4.6.1
  * Switch to debhelper level 13
  * Use salsa.debian.org Vcs-* URLs
  * Track and download the news releases from GitHub

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 11 Oct 2022 17:13:13 +0200

maven-source-plugin (3.0.1-2+apertis1) apertis; urgency=medium

  * Set component to sdk. Move java packages to sdk to avoid building
    for arm architecture.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 22 Feb 2022 17:22:33 +0530

maven-source-plugin (3.0.1-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Wed, 17 Feb 2021 21:16:27 +0000

maven-source-plugin (3.0.1-2) unstable; urgency=medium

  * Team upload.
  * Replace dependencies on libplexus-utils-java with libplexus-utils2-java.
  * Update copyright info.

 -- Miguel Landaeta <nomadium@debian.org>  Thu, 24 Aug 2017 17:53:58 +0100

maven-source-plugin (3.0.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Depend on libmaven3-core-java instead of libmaven2-core-java
  * Standards-Version updated to 4.0.0
  * Switch to debhelper level 10
  * Use secure Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 18 Jul 2017 22:46:14 +0200

maven-source-plugin (2.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Moved the package to Git
  * Standards-Version updated to 3.9.6 (no changes)
  * Removed the javadoc package
  * Build with the DH sequencer instead of CDBS

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 09 Dec 2015 23:13:57 +0100

maven-source-plugin (2.2.1-1) unstable; urgency=low

  * Team upload.
  * New upstream release
  * Enabled the unit tests
  * debian/watch: Updated to catch the latest versions
  * debian/control: Updated Standards-Version to 3.9.5 (no changes)

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 05 Nov 2013 15:31:09 +0100

maven-source-plugin (2.1.2-1) unstable; urgency=low

  * Team upload.
  * New upstream release
  * Fixed a build failure when installing the Javadoc in the doc package
  * debian/control:
    - Use canonical URLs for the Vcs-* fields
    - Wrap and sort the dependencies
  * debian/copyright: Updated the Format URI to 1.0
  * debian/watch: Updated to catch the latest 2.x versions
  * debian/orig-tar.sh: Use XZ compression when generating the upstream tarball

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 04 Sep 2013 16:19:32 +0200

maven-source-plugin (2.0.4-1) unstable; urgency=low

  * Initial release (Closes: #691927)
  * demoted to version 2.0 vs the current 2.2 because of build
    dependency to maven-plugin-tools by Steffen Moeller.
    - junit version 3 

 -- Thomas Koch <thomas@koch.ro>  Wed, 31 Oct 2012 15:34:36 +0100
